<?php
declare(strict_types=1);

namespace Drupal\ksitemap\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\ksitemap\KSiteMapInterface;

/**
 * Class Ksitemap
 * @package Drupal\ksitemap\Entity
 * Defines the site map entity
 * @ConfigEntityType(
 *     id = "ksitemap",
 *     label = @Translation("KSiteMap"),
 *     handlers = {
 *       "storage" = "Drupal\ksitemap\KSiteMapStorage",
 *       "list_builder" = "Drupal\ksitemap\KSiteMapListBuilder",
 *       "form" = {
 *         "add" = "Drupal\ksitemap\Form\KSiteMapForm",
 *         "edit" = "Drupal\ksitemap\Form\KSiteMapForm",
 *         "delete" = "Drupal\ksitemap\Form\KSiteMapDeleteForm"
 *       }
 *     },
 *     config_prefix = "ksitemap",
 *     admin_permission = "administer site configuration",
 *     entity_keys = {
 *       "id" = "id",
 *       "uuid" = "uuid",
 *       "label" = "label"
 *     },
 *     links = {
 *       "edit-form" = "/admin/config/search/ksitemap/{ksitemap}/edit",
 *       "delete-form" = "admin/config/search/ksitemap/{ksitemap}/delete"
 *     }
 *   }
 * )
 */
class KSiteMap extends ConfigEntityBase implements KSiteMapInterface
{
    /** @var int */
    private $id;
    /** @var string */
    private $title;
    /** @var string */
    private $address;
    /** @var \DateTime */
    private $lastUpdated;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @return \DateTime
     */
    public function getLastUpdated()
    {
        return $this->lastUpdated;
    }
}