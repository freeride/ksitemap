<?php
declare(strict_types=1);

namespace Drupal\ksitemap;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

interface KSiteMapInterface extends ConfigEntityInterface
{
    /**
     * @return int
     */
    public function getId();

    /**
     * @return string
     */
    public function getTitle();

    /**
     * @return string
     */
    public function getAddress();

    /**
     * @return \DateTime
     */
    public function getLastUpdated();
}